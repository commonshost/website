import TemplateElement from '../TemplateElement.js'
import { Client } from '../../auth0-api.js'
import { PATCH } from '../../api.js'

class Profile extends TemplateElement {
  constructor () {
    super()
    this.template = '/scripts/components/account/profile.html'
    this.userId = ''
  }

  async connectedCallback () {
    await super.connectedCallback()
    const root = this.shadowRoot

    root.querySelector('#email')
      .addEventListener('submit', this._changeEmail.bind(this))
    root.querySelector('#password')
      .addEventListener('submit', this._changePassword.bind(this))
    root.querySelector('#session')
      .addEventListener('submit', this._logout.bind(this))

    root.querySelector('#password')
      .addEventListener('input', (event) => {
        const password = root.querySelector('#password #new')
        const confirm = root.querySelector('#password #confirm')
        if (confirm.value === password.value) {
          confirm.setCustomValidity('')
        } else {
          confirm.setCustomValidity('Password confirmation must match.')
        }
      })

    const auth0 = new Client()
    try {
      const profile = await auth0.profile()
      this.userId = profile.sub
      const username = profile.nickname || profile.name
      root.querySelector('#email #email-update').value = profile.email
      root.querySelector('#session #authenticated').removeAttribute('hidden')
      root.querySelector('#session #session-username').value = username
      root.querySelector('#session #session-email').value = profile.email
      root.querySelector('#session #session-picture').src = profile.picture
    } catch (error) {
      root.querySelector('#session #problem').removeAttribute('hidden')
      root.querySelector('#session #error').textContent = error.message
    } finally {
      root.querySelector('#session #loading').setAttribute('hidden', true)
    }
  }

  async _changeEmail (event) {
    event.preventDefault()
    const root = this.shadowRoot
    const email = root.querySelector('#email #email-update').value
    root.querySelector('#email #pending').removeAttribute('hidden')
    root.querySelector('#email #success').setAttribute('hidden', true)
    root.querySelector('#email #error').setAttribute('hidden', true)
    try {
      const url = `/v2/users/${encodeURIComponent(this.userId)}`
      const response = await PATCH(url, { email })
      if (response.ok === false) {
        throw new Error('Failed to update email address.')
      }
      root.querySelector('#email #success').removeAttribute('hidden')
    } catch (error) {
      root.querySelector('#email #error').removeAttribute('hidden')
      root.querySelector('#email #message').textContent = error.message
    } finally {
      root.querySelector('#email #pending').setAttribute('hidden', true)
    }
  }

  async _changePassword (event) {
    event.preventDefault()
    const root = this.shadowRoot
    const password = root.querySelector('#password #new').value
    root.querySelector('#password #pending').removeAttribute('hidden')
    root.querySelector('#password #success').setAttribute('hidden', true)
    root.querySelector('#password #error').setAttribute('hidden', true)
    try {
      const url = `/v2/users/${encodeURIComponent(this.userId)}`
      const response = await PATCH(url, { password })
      if (response.ok === false) {
        throw new Error('Failed to update password.')
      }
      root.querySelector('#password #success').removeAttribute('hidden')
      root.querySelector('#password #new').value = ''
      root.querySelector('#password #confirm').value = ''
    } catch (error) {
      root.querySelector('#password #error').removeAttribute('hidden')
      root.querySelector('#password #message').textContent = error.message
    } finally {
      root.querySelector('#password #pending').setAttribute('hidden', true)
    }
  }

  async _logout (event) {
    event.preventDefault()

    const localStorageKeys = []
    for (let i = 0; i < localStorage.length; i++) {
      const key = localStorage.key(i)
      localStorageKeys.push(key)
    }
    for (const key of localStorageKeys) {
      if (key.startsWith('app.')) {
        localStorage.removeItem(key)
      }
    }

    window.history.pushState(null, '', '/kthxbye')
    window.dispatchEvent(new PopStateEvent('popstate'))
  }
}

window.customElements.define('app-account-profile', Profile)
export default Profile
