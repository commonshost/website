const help = `
<style>
  @import '/styles/fonts.css';

  code {
    font-family: var(--font-stack-monospace);
    font-size: 16px;
    color: black;
    white-space: pre-wrap;
  }
</style>
<code>$ npx @commonshost/cli --help
Usage: commonshost <command>

Commands:
  config  Edit user settings  [aliases: configure, configuration, setting, settings, option, options]
  delete  Stop hosting a site  [aliases: rm, destroy, del, unpublish]
  deploy  Make files available on the service  [aliases: publish, serve, sync, upload]
  list    Show hosted sites  [aliases: ls, ll, la, l]
  login   Authenticate an account  [aliases: signin]
  logout  Clear the session token  [aliases: signout]
  signup  Create an account  [aliases: register]
  whoami  Show user information

Options:
  -h, --help     Show help  [boolean]
  -v, --version  Show version number  [boolean]

Examples:
  commonshost --help <command>  Show command-specific options

Documentation:
  https://gitlab.com/commonshost/cli#README
</code>`

const template = document.createElement('template')
template.innerHTML = help

class CliHelp extends HTMLElement {
  connectedCallback () {
    const root = this.attachShadow({ mode: 'open' })
    const clone = document.importNode(template.content, true)
    root.appendChild(clone)
  }
}

window.customElements.define('app-cli-help', CliHelp)
export default CliHelp
