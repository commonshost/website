import TemplateElement from './TemplateElement.js'

class NotFound extends TemplateElement {
  constructor () {
    super()
    this.template = '/scripts/components/404.html'
  }

  async connectedCallback () {
    await super.connectedCallback()
    const root = this.shadowRoot

    const emojis = [
      '🤷‍♂️', '🤷🏻‍♂️', '🤷🏼‍♂️', '🤷🏽‍♂️', '🤷🏾‍♂️', '🤷🏿‍♂️',
      '🤷‍♀️', '🤷🏻‍♀️', '🤷🏼‍♀️', '🤷🏽‍♀️', '🤷🏾‍♀️', '🤷🏿‍♀️',
      '💩'
    ]
    const random = emojis[Math.floor(Math.random() * emojis.length)]
    root.querySelector('h2').textContent = random
  }
}

window.customElements.define('app-404', NotFound)
export default NotFound
