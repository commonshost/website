const origin = 'https://api.commons.host:8888'

async function request (method, endpoint, headers, body) {
  const url = `${origin}${endpoint}`
  const accessToken = localStorage.getItem('app.accessToken')
  const options = {
    method,
    body: JSON.stringify(body),
    headers: Object.assign({
      'content-type': 'application/json',
      authorization: `Bearer ${accessToken}`
    }, headers)
  }
  const response = await fetch(url, options)
  const contentType = response.headers.get('content-type') || ''
  if (!contentType.startsWith('application/json')) {
    throw new Error(await response.text())
  }
  const data = await response.json()
  if (response.ok === false) {
    throw new Error(data.message || response.statusText)
  }
  return data
}

export async function GET (endpoint, headers) {
  return request('GET', endpoint, headers)
}

export async function DELETE (endpoint, headers) {
  return request('DELETE', endpoint, headers)
}

export async function POST (endpoint, body, headers) {
  return request('POST', endpoint, headers, body)
}

export async function PATCH (endpoint, body, headers) {
  return request('PATCH', endpoint, headers, body)
}

export async function PUT (endpoint, body, headers) {
  return request('PUT', endpoint, headers, body)
}
